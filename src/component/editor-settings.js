import React, { Component } from 'react'
import { Select } from 'antd';

const { Option } = Select;
class EditorSetting extends Component {
    onChange(value) {
        this.props.getValue(value)
        console.log(`selected ${value}`);
    }
    render() {
        return (
            <Select
                defaultValue={this.props.defaultValue}
                placeholder={this.props.placeholder}
                style={{ width: "100%" }}
                onChange={(value)=>this.props.handleSettings(this.props.placeholder, value)}
            >
                {this.props.values.map((val) =>
                    <Option value={val} > {val} </Option>
                )}
            </Select>
        )
    }
}

export default EditorSetting