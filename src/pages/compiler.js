import React, { Component } from 'react'
import Editor from '../component/editor'
import SideNav from '../component/sidenav'
import Result from '../component/result'
class Compiler extends Component {
    state = {
        theme : "monokai",
        enableBasicAutocompletion : true,
        language:false,
        enableLiveAutocompletion :true,
        enableSnippets:true,
        highlightActiveLine : false,
        fontSize:14
    }
    handleSettings = (key,val) => {
        let x = {}
        x[key] = val
        this.setState(x)
    }
    render() {
        return (
            <div style={{ display: "flex", width: "100%" }} >
                <SideNav handleSettings = {this.handleSettings} />
                <div style={{ width: "100%" }}>
                    <Editor properties={this.state} />
                   <div style={{margin:"10px"}}>
                   <Result></Result>
                   </div>
                </div>
            </div>
        )
    }
}

export default Compiler