import React, { Component } from 'react'
import { Tabs } from 'antd';

const { TabPane } = Tabs;


class Result extends Component {
    callback(key) {
        console.log(key);
    }
    render() {
        return (
            <Tabs defaultActiveKey="1" onChange={this.callback}>
                {/* <TabPane tab="Compile" key="1">
                    Content of Tab Pane 1
            </TabPane> */}
                <TabPane tab="Run Sample test" key="1">
                    Content of Tab Pane 2
            </TabPane>
                {/* <TabPane tab="Submit" key="3">
                    Content of Tab Pane 3
            </TabPane> */}
            </Tabs>
        )
    }
}

export default Result