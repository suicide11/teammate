var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cors = require('cors')
var http = require('http');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);



app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors())
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
const { stringify } = require('querystring');

// routes
app.use('/api/v1/', indexRouter);

app.use(function (req, res, next) {
  next(createError(404));
});

io.on('connection', function (socket) {
  var query = socket.handshake.query;
  console.log(query)
  var roomName = query.roomName;
  // if (!roomName) {
  //   console.log("yes")
  //   io.emit('change', { message: query.roomName });
  // }
  // io.emit('change', { message: query.roomName });
  io.sockets.in(query.roomName).emit('newUserJoined', { message: "hello" });
  socket.join(roomName);
  socket.on('event', function (data) {
    console.log(data)
    io.to(data.room).emit('change', { message: data.message });
  });
  socket.on('newUserJoined', function (data) {
    console.log(data)
    io.to(data.room).emit('change', { message: data.message });
  });
});

app.use(function (err, req, res, next) {
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  res.status(err.status || 500);
  res.render('error');
});

server.listen(8000, function () {
  console.log('Express server listening on port ' + 8000)
});
