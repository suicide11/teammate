import React,{Component} from 'react'
import AceEditor from "react-ace";
import axios from 'axios'
import "ace-builds/src-noconflict/mode-python";
import "ace-builds/src-noconflict/mode-java"
import "ace-builds/src-noconflict/mode-javascript"
import "ace-builds/src-noconflict/mode-c_cpp"
import "ace-builds/src-noconflict/theme-monokai";
import "ace-builds/src-noconflict/theme-github";
import "ace-builds/src-noconflict/theme-dracula";
import "ace-builds/src-noconflict/ext-language_tools"
import io from 'socket.io-client'
const ENDPOINT = "http://localhost:8000/"
const roomName =  window.location.href.substr(window.location.href.lastIndexOf('/') + 1);
var socket = io.connect(ENDPOINT,{query: {
    roomName: roomName,
}});

class Editor extends Component {
    state = {
        code: "",
        output: false
    }
    componentWillMount() {
        socket.on('change',function(data) {
            console.log(data)
            this.setState({
                code:data.message
            })
        }.bind(this))
        socket.on('newUserJoined',function(data) {
            socket.emit('newUserJoined', { message:this.state.code, room:roomName});
        }.bind(this))
    }
    onChange = (value) => {
        this.setState({
            code: value
        })
        socket.emit('event', { message:value, room:roomName});
    }
    onLoad = (value) => {
        console.log(value)
    }
    handleSubmit = () => {
        axios.post('http://localhost:8000/api/v1', {
            code: this.state.code
        })
            .then(function (response) {
                this.setState({ output: response.data })
            }.bind(this))
            .catch(function (error) {
                console.log(error);
            });
    }
    handleRoom = () => {
        socket.emit('join', { room:"2mDwiLtsAGV8zB79AAAA"});
    }
    render() {
        console.log(this.props)
        return (
            <div style={{width:"100%"}} >
                <AceEditor
                        placeholder="Write your code here"
                        mode={this.props.properties.language}
                        theme={this.props.properties.theme}
                        name="blah2"
                        value ={this.state.code}
                        onLoad={this.onLoad}
                        onChange={this.onChange}
                        fontSize={this.props.properties.fontSize}
                        showPrintMargin={false}
                        showGutter={true}
                        highlightActiveLine={false}
                        editorProps={{ $blockScrolling: true }}
                        setOptions={{
                            enableBasicAutocompletion: true,
                            enableLiveAutocompletion: true,
                            enableSnippets: true
                        }}
                    />
            </div>
        )
    }
}


export default Editor