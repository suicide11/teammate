import React, { Component } from 'react'
import { Menu } from 'antd';
import { AppstoreOutlined, MailOutlined, SettingOutlined } from '@ant-design/icons';
import EditorSetting from './editor-settings'
import { Checkbox } from 'antd';
const { SubMenu } = Menu;

class SideNav extends Component {
    handleClick = e => {
        console.log('click ', e);
    };
    onChange(e) {
        console.log(`checked = ${e.target.checked}`);
    }
    render() {
        return (
            <div
                style={{ position: "sticky", borderRight: "solid #f0f0f0 1px", left: "0", top: "0", height: "100vh", width: 256, padding: "15px 10px" }}
            >
                <div className="settings" >
                    <br />
                    <center>
                        <EditorSetting 
                            defaultValue={"monokai"} 
                            placeholder={"theme"}
                            values={["monokai", "github", "dracula"]}
                            handleSettings = {this.props.handleSettings}
                        />
                    </center>
                </div>
                <div className="settings">
                    <br />
                    <center>
                        <EditorSetting  
                            placeholder={"language"} 
                            values={["c", "c++", "javaScript", "python", "java"]} 
                            handleSettings = {this.props.handleSettings}
                        />
                    </center>
                </div>
                <div className="settings">
                    <br />
                    <center>
                        <EditorSetting 
                            defaultValue = {14} 
                            placeholder={"fontSize"} 
                            values={[10, 12, 14, 16, 18, 20]} 
                            handleSettings = {this.props.handleSettings}
                        />
                    </center>
                </div>
                <div className="settings">
                    <br />
                    <Checkbox 
                        onChange={(e)=>this.props.handleSettings("highlightActiveLine",e.target.checked)}
                    >
                        Highlight active line
                    </Checkbox>
                </div>
                <div className="settings">
                    <br />
                    <Checkbox 
                        defaultChecked 
                        onChange={(e)=>this.props.handleSettings("enableLiveAutocompletion",e.target.checked)}
                    >
                        Auto complete
                    </Checkbox>
                </div>
            </div>
        );
    }
}

export default SideNav