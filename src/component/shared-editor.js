import React, { Component } from 'react'
import AceEditor from "react-ace";
import axios from 'axios'
import "ace-builds/src-noconflict/mode-python";
import "ace-builds/src-noconflict/theme-monokai";
import "ace-builds/src-noconflict/ext-language_tools"
import io from 'socket.io-client'
const ENDPOINT = "http://localhost:8000/"
const roomName =  window.location.href.substr(window.location.href.lastIndexOf('/') + 1);
var socket = io.connect(ENDPOINT,{query: {
    roomName: roomName,
}});

class SharedEditor extends Component {
    state = {
        code: "",
        output: false
    }
    componentWillMount() {
        socket.on('change',function(data) {
            console.log(data)
            this.setState({
                code:data.message
            })
        }.bind(this))
    }
    onChange = (value) => {
        this.setState({
            code: value
        })
        socket.emit('event', { message:value, room:roomName});
    }
    onLoad = (value) => {
        console.log(value)
    }
    handleSubmit = () => {
        axios.post('http://localhost:8000/api/v1', {
            code: this.state.code
        })
            .then(function (response) {
                this.setState({ output: response.data })
            }.bind(this))
            .catch(function (error) {
                console.log(error);
            });
    }
    handleRoom = () => {
        socket.emit('join', { room:"2mDwiLtsAGV8zB79AAAA"});
    }
    render() {
        return (
            <React.Fragment>
                <div className="editor" >
                    <AceEditor
                        placeholder="Placeholder Text"
                        mode="python"
                        theme="monokai"
                        name="blah2"
                        value ={this.state.code}
                        onLoad={this.onLoad}
                        onChange={this.onChange}
                        fontSize={14}
                        showPrintMargin={false}
                        showGutter={true}
                        highlightActiveLine={false}
                        editorProps={{ $blockScrolling: true }}
                        setOptions={{
                            enableBasicAutocompletion: true,
                            enableLiveAutocompletion: true,
                            enableSnippets: true
                        }}
                    />
                    <div>
                        
                    </div>
                </div>
                <button onClick={this.handleSubmit} > Sumbit </button>
                {/* <button onClick={this.handleRoom} > Sumbit </button> */}
                {this.state.output}
            </React.Fragment>
        )
    }
}

export default SharedEditor