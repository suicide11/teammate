const { spawn } = require('child_process');
var fs = require('fs');
const { v4: uuidv4 } = require('uuid');
var io = require('socket.io');
exports.editor = (req, res) => {
    const id = uuidv4()
    fs.mkdirSync("./codes/python/" + id);

    fs.writeFile("./codes/python/" + id + "/test.py", req.body.code, async (err) => {
        console.log(uuidv4())
        if (err) {
            return console.log(err);
        }
        fs.writeFile("./codes/python/" + id + "/input.txt", "4\n4", async (err) => {
            console.log(uuidv4())
            if (err) {
                return console.log(err);
            }
        })
        let error = false
        let output = false
        var child = spawn('python', ["./codes/python/" + id + "/test.py"]);
        await child.stderr.on('data', function (data) {
            process.stdout.write(data.toString());
            error = data.toString()
        });
        await child.stdout.on('data', function (data) {
            output = data.toString()
        });

        await child.on('close', function (code) {
            if (error) {
                return res.send(error)
            }
            return res.send(output)
        });
    });
}